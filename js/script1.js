$( function() {
  $('.text1').show();
  $('.tab1').addClass('active')

  $('.tab1').click( function() {
    noneDisplay();
    $('.text1').show();
    $('.tab1').addClass('active')
  });

  $('.tab2').click( function() {
    noneDisplay();
    $('.text2').show();
    $('.tab2').addClass('active')
  })

  $('.tab3').click( function() {
    noneDisplay();
    $('.text3').show();
    $('.tab3').addClass('active')
  })
});

function noneDisplay() {
  $('.text1').css('display', 'none');
  $('.text2').css('display', 'none');
  $('.text3').css('display', 'none');

  $('.tab1').removeClass('active');
  $('.tab2').removeClass('active');
  $('.tab3').removeClass('active');
}
