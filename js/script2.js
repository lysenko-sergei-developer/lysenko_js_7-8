$( function (){
  $('.input__firstname').hover( function () {
    $('.help__firstname').fadeIn(350);
  });

  $('.input__lastname').hover( function() {
    $('.help__lastname').fadeIn(350);
  });

  $('.input__address').hover( function() {
    $('.help__address').fadeIn(350);
  });

  $('.input__firstname').on('mouseleave', function () {
    $('.help__firstname').fadeOut(350);
  });

  $('.input__lastname').on('mouseleave', function() {
    $('.help__lastname').fadeOut(350);
  });

  $('.input__address').on('mouseleave', function() {
    $('.help__address').fadeOut(350);
  });

  $('.help-button').on('click', function(e) {
    $('.help__firstname').fadeIn(350);
    $('.help__lastname').fadeIn(350);
    $('.help__address').fadeIn(350);
  });
});
